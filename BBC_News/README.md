# Text Classification

Author: Lê Minh Khôi

Dataset: [BBC News](https://www.kaggle.com/c/learn-ai-bbc/data)

Algorithm:
- TF-IDF as feature extraction techinique
- Random Forest and Decision Tree as classifier

Conclusion: Random Forest outperforms Decision Tree with overall accuracy of 96% compare to 84%
