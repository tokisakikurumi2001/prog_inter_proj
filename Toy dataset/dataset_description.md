# Data Set
## 1. Data Set information
The Car Evaluation Dataset was derived from a simple hierarchical decision model originally developed for the demonstration of DEX, M.Bohanec, V.Rajkovic. The model evaluates cars according to the following concept structure:
- CAR: car acceptability
- PRICE: overall price
- buying: buying price
- maint: price of maintenance
- TECH: technical characteristics
- COMFORT: comfort
- doors: number of doors
- persons: capacity in terms of persons to carry
- lug_boot: the size of luggage boot
- safety: estimated safety of the car

Input attributes are printed in lowercase, Besides the largest concept (CAR), the model includes 3 intermediate concepts: PEICE, TECH, COMFORT.

The Car Evaluation Database contains examples with the structural information removed, which directly relates to CAR to the 6 input attributes: buying, maint, doors, persons, lug_boot, safety.

## 2. Attribute information
Class values: unacc, good, vgood
Attributes:
- buying: vhigh, high, med, low
- maint: vhigh, high, med, low
- doors: 2, 3, 4, 5more
- persons: 2, 4, more
- lug_boot: small, med, big
- safety: low, med, high